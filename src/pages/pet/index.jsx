import { IonContent, IonPage, IonPopover } from '@ionic/react';
import React, { useState } from 'react';
import FooterNav from './common/footerNav';
import { Link } from 'react-router-dom';
import Popover from '@material-ui/core/Popover';

//ICONS
import { GoKebabVertical } from "react-icons/go";
import { IoMdArrowRoundBack } from "react-icons/io";

//STYLES
import './styles.scss';

function Pet() {
  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const open = Boolean(anchorEl);
  const id = open ? 'simple-popover' : undefined;

  return (
    <IonPage>
      <div id="header-pet">
          <Link className="left" to="/home" >
            <IoMdArrowRoundBack/>
          </Link>
          <div className="right" aria-describedby={id} variant="contained" color="primary" onClick={handleClick} >
            <GoKebabVertical />
          </div>
          <Popover
            id={id}
            open={open}
            anchorEl={anchorEl}
            className="dropdown"
            onClose={handleClose}
            anchorOrigin={{
              vertical: 'bottom',
              horizontal: 'center',
            }}
            transformOrigin={{
              vertical: 'top',
              horizontal: 'center',
            }}
          >
          <ul>
            <li>Mi perfil</li>
            <li>Veterinarios</li>
            <li>Más cosas</li>
            <li>Otra cosa</li>
          </ul>
        </Popover>
      </div>
      <IonContent fullscreen>
        <FooterNav />
      </IonContent>
    </IonPage>
  );
};

export default Pet;
