import React from 'react';
import { makeStyles, Theme } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tab from '@material-ui/core/Tab';
import TabContext from '@material-ui/lab/TabContext';
import TabList from '@material-ui/lab/TabList';
import TabPanel from '@material-ui/lab/TabPanel';

//ICONS
import { IoPawOutline } from "react-icons/io5";
import { BiCalendar } from "react-icons/bi";
import { FaPills } from "react-icons/fa";
import { HiOutlineLocationMarker } from "react-icons/hi";

//PAGES
import MyPet from '../mypet/index';

//STYLES
import './styles.scss';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
  },
}));

export default function FooterNav() {
  const classes = useStyles();
  const [value, setValue] = React.useState('1');

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <div className={classes.root}>
      <TabContext value={value}>
        <TabPanel value="1"><MyPet /></TabPanel>
        <TabPanel value="2">Item Two</TabPanel>
        <TabPanel value="3">Item Three</TabPanel>
        <TabPanel value="4">Item Three</TabPanel>
        <AppBar className="footer-nav" position="static">
          <TabList onChange={handleChange} aria-label="simple tabs example">
            <Tab label="My Pet" value="1" icon={<IoPawOutline />} />
            <Tab label="Calendar" value="2" icon={<BiCalendar />} />
            <Tab label="Veterinario" value="3" icon={<FaPills />} />
            <Tab label="Location" value="4" icon={<HiOutlineLocationMarker />} />
          </TabList>
        </AppBar>
      </TabContext>
    </div>
  );
}
