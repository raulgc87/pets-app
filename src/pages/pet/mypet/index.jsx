import React from 'react';

//ICONS
import { FiEdit } from "react-icons/fi";
import { IoMdArrowRoundBack } from "react-icons/io";

//STYLES
import './styles.scss';

function MyPet() {

  function editandoPerfil() {
    console.log('Nos vamos a editar cosas');
  }

  return (
    <>
    <div className="pet-picture">
      <img src="./assets/img/perrete.jpg" alt=""/>
    </div>
    <div className="content-pet">
      <h1>Willy</h1>
      <FiEdit className="edit-icon" onClick={ editandoPerfil } />
      <span className="raza">Podenco con Bodeguero</span>
      <div className="kpi">
        <div className="kpi-1">
          <span className="result">Male</span>
          <span className="kind-of">Gender</span>
        </div>
        <div className="kpi-2">
          <span className="result">2 years</span>
          <span className="kind-of">Age</span>
        </div>
        <div className="kpi-3">
          <span className="result">16,5Kg</span>
          <span className="kind-of">Weight</span>
        </div>
      </div>
      <div className="curious">
        <div className="comment">
          <div className="title">
            You know that…
          </div>
          <div className="desc">
            A bored dog can eat sticks. Dogs need physical and mental exercise to avoid these bad behaviors.
          </div>
        </div>
        <div className="image__comment">
          <img src="./assets/img/perrete1.png" alt=""/>
        </div>
      </div>
    </div>
    </>
  );
};

export default MyPet;
