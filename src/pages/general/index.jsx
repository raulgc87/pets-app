import { IonPage } from '@ionic/react';
import { Redirect, Route } from 'react-router-dom';
import { IonRouterOutlet } from '@ionic/react';
import { IonReactRouter } from '@ionic/react-router';
import Home from './home/Home';
import Login from './login/index';
import Pet from '../pet/index';
import newUser from './login/newUser';

function General() {
  return (
    <IonPage>
        <IonReactRouter>
        <IonRouterOutlet>
            <Route exact path="/login"> <Login /> </Route>
            <Route exact path="/new-user"> <newUser /> </Route>
            <Route exact path="/home"> <Home /> </Route>
            <Route exact path="/pet"> <Pet /> </Route>
            <Route exact path="/"> <Redirect to="/login" /> </Route>
        </IonRouterOutlet>
        </IonReactRouter>
    </IonPage>
  );
};

export default General;