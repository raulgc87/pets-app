import React from 'react';
import { makeStyles, Theme } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tab from '@material-ui/core/Tab';
import TabContext from '@material-ui/lab/TabContext';
import TabList from '@material-ui/lab/TabList';
import TabPanel from '@material-ui/lab/TabPanel';
import { Link } from 'react-router-dom';

//ICONS
import { RiHomeHeartLine } from "react-icons/ri";
import { BiCalendar } from "react-icons/bi";
import { FaPills } from "react-icons/fa";
import { HiOutlineTicket } from "react-icons/hi";
import { RiAddCircleFill } from "react-icons/ri";

//Pages
import MyEventCalendar from '../calendar/index.jsx';

//STYLES
import './styles.scss';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
  },
}));

export default function MainNav() {
  const classes = useStyles();
  const [value, setValue] = React.useState('1');

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  function addPet() {
    console.log('adding a new pet action')
  }

  return (
    <div className={classes.root}>
      <TabContext value={value}>
        <TabPanel value="1">
            <ul className="mypet-list">
              <li className="mascota">
                <Link to="/pet" >
                    <img src="./assets/img/perrete.jpg" alt=""/>
                    <div className="pet-name">Willy</div>
                </Link>
              </li>
              <li className="mascota">
                <Link to="/pet" >
                    <img src="./assets/img/perrete.jpg" alt=""/>
                    <div className="pet-name">Willy</div>
                </Link>
              </li>
              <li className="mascota">
                <Link to="/pet" >
                    <img src="./assets/img/perrete.jpg" alt=""/>
                    <div className="pet-name">Willy</div>
                </Link>
              </li>
              <li className="mascota add-pet" onClick={ addPet }>
                <RiAddCircleFill />
                <span>Add New Pet</span>
              </li>
            </ul>
        </TabPanel>
        <TabPanel value="2"> <MyEventCalendar /> </TabPanel>
        <TabPanel value="3">Item Three</TabPanel>
        <TabPanel value="4">Item Three</TabPanel>
        <AppBar className="footer-nav" position="static">
          <TabList onChange={handleChange} aria-label="simple tabs example">
            <Tab label="Home" value="1" icon={<RiHomeHeartLine />} />
            <Tab label="Calendar" value="2" icon={<BiCalendar />} />
            <Tab label="Veterinario" value="3" icon={<FaPills />} />
            <Tab label="Cupons" value="4" icon={<HiOutlineTicket />} />
          </TabList>
        </AppBar>
      </TabContext>
    </div>
  );
}
