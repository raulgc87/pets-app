import { IonContent, IonHeader, IonPage, IonTitle, IonToolbar } from '@ionic/react';
import { Link } from 'react-router-dom';

import MainNav from '../common/mainNav';

//ICONS
import { MdPets } from "react-icons/md";

import './Home.scss';

function Home() {
  return (
    <IonPage>
      <IonContent fullscreen>
          <div className="logo"><MdPets />Petlify</div>
          < MainNav />
      </IonContent>
    </IonPage>
  );
};

export default Home;