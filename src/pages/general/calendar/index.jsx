import React from 'react';
import { IonCard, IonCardContent } from '@ionic/react';
import { RiSurroundSoundLine } from 'react-icons/ri';

export default function MyEventCalendar() {

  const URL = fetch('http://calapi.inadiutorium.cz/api/v0/en/calendars/general-en/yesterday').then(res => res.json());
    
  console.log(URL);

  return (
    <IonCard>
      <IonCardContent>
            Keep close to Nature's heart... and break clear away, once in awhile,
            and climb a mountain or spend a week in the woods. Wash your spirit clean.
      </IonCardContent>
    </IonCard>
  );
}