import React from 'react';
import { useForm } from 'react-hook-form';
import { IonContent, IonPage } from '@ionic/react';
import { Link } from 'react-router-dom';
import './styles.scss';

//ICONS
import { MdPets } from "react-icons/md";

export default function Login() {

  //getValues();

  const { register, handleSubmit, formState: { errors } } = useForm();
  const onSubmit = data => {
    localStorage.setItem('login', JSON.stringify(data));
  }
  
  function getValues() {
    setTimeout(function(){
      const stored = localStorage.getItem('login');
      const arrayStored = JSON.parse(stored);
      if (arrayStored.Email == 'pet@pet.com' && arrayStored.Password == 'pet') {
        console.log('true');
        window.location.href = "/home";
      } else {
        console.log('false');
      }
    }, 500);
  }

  return (
    <IonPage>
      <IonContent fullscreen>
        <div className="login-page">
          <div className="logo"><MdPets />Petlify</div>
          <form onSubmit={handleSubmit(onSubmit)}>
            <input type="email" placeholder="Email" {...register("Email", {required: true, pattern: /^\S+@\S+$/i})} />
            <input type="password" placeholder="Password" {...register("Password", {required: true, minLength: 1, maxLength: 12})} />
            <input type="submit" onClick={ getValues }/>
          </form>
        </div>
      </IonContent>
    </IonPage>
  );
}